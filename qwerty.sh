#!/bin/bash

plot1=".    .        .      .             . .     .        .          .          .
         .                 .                    .                .
  .               A long time ago in a galaxy far, far away...   .
     .               .           .               .        .             .
     .      .            .                 .                                                .
 .      .         .         .   . :::::+::::...      .          .                      .
     .         .      .    ..::.:::+++++:::+++++:+::.    .     .
                        .:.  ..:+:..+|||+..::|+|+||++|:.             .                          .
            .   .    :::....:::::::::++||||O||O#OO|OOO|+|:.    .
.      .      .    .:..:..::+||OO#|#|OOO+|O||####OO###O+:+|+                    .
                 .:...:+||O####O##||+|OO|||O#####O#O||OO|++||:     .    .
  .             ..::||+++|+++++|+::|+++++O#O|OO|||+++..:OOOOO|+  .                      .
     .   .     +++||++:.:++:..+#|. ::::++|+++||++O##O+:.++|||#O+    .
.           . ++++++++...:+:+:.:+: ::..+|OO++O|########|++++||##+                                   .
  .       .  :::+++|O+||+::++++:::+:::+++::+|+O###########OO|:+OO       .  .
     .       +:+++|OO+|||O:+:::::.. .||O#OOO||O||#@###@######:+|O|  .
 .          ::+:++|+|O+|||++|++|:::+O#######O######O@############O
          . ++++: .+OO###O++++++|OO++|O#@@@####@##################+                 .
      .     ::::::::::::::::::::++|O+..+#|O@@@@#@###O|O#O##@#OO####     .
 .        . :. .:.:. .:.:.: +.::::::::  . +#:#@:#@@@#O||O#O@:###:#| .      .
                           .  .:.:.:.:. . :.:.:%::%%%:::::%::::%:::
.      .                                       .:.:.:.:   :.:.:.:.  .   .
           .                       .     .                      .                                    .   
      .
.          .                        .     .   .                                    .   .
                                                                             .
    .        .                                                           .
    .     .   .                .            .     .   . 
   .     .  
     "

plot2="\033[1m                                                                 .
              .   A terrible civil war burns throughout the  .        .     .              .     
                 git: a rag-tag group of RaspberryPi fighters   .  .
     .       .  has risen from beneath the dark shadow of the evil           .
.        .     monster the Microsoft has become.                  .                 .       .     .
   .             Bill's  forces  have  instituted  a reign of windows,  .      .
             and every  weapon in its arsenal has  been turned upon the 
          . Rebels  and  their  allies: small fonts, le-baruurs,overridden .   .
   .       merges, overwhelming containers, and qwerty. Fear keeps the individual        .           .
.    .  . systems on the test branches, and is the prime motivator of the LinkedIn.   .
                      .
    .    Outnumbered and outgunned,  the Raspberry Pi burns across the vast reaches .    .
.       of git and thousand-thousand branches, with only their great courage - and the               .
    .  mystical power known as the YAMS - Yet (Another) Alex M Schapelle - flaming a fire  .
      of hope.
                                         .
    This is a galaxy of wondrous repos, bizarre comments, strange Droids, powerful LAMP(hp)s         .
 . great heroes, and terrible villains.  It is a galaxy of fantastic worlds, magical devices, 
  vast fleets, awesome machinery, terrible conflict, and unending hope.  \033[0m"

plot3="    .         .
.        .          .    .    .            .            .                   .
               .               ..       .       .   .             .
 .      .     T h i s   i s   t h e   g a l a x y   o f   . . .             .
                     .              .       .                    .                              .
.        .               .       .     .            .
   .           .        .                     .        .            .
             .               .    .          .              .   .       .        .            .      .
               _________________      ____         __________
 .       .    /                 |    /    \    .  |          "'\'"
     .       /    ______   _____| . /      \      |    ___    |     .     .
             \    \    |   |       /   /\   \     |   |___|   |
           .  \    \   |   |      /   /__\   \  . |         _/                                       .      
 .     ________]    |  |   | .   /            \   |   |\    \_______    .       .       .
      |            /   |   |    /    ______    \  |   | \           |
      |___________/    |___|   /____/      \____\ |___|  \__________|    .
  .     ____    __  . _____   ____      .  __________   .  _________                .             .
       \    \  /  \  /    /  /    \       |          \    /         |      .
        \    \/    \/    /  /      \      |    ___    |  /    ______|  .
         \              /  /   /\   \ .   |   |___|   |  \    "'\'"
   .      \            /  /   /__\   \    |         _/.   \    "'\'"            .       .     .
           \    /\    /  /            \   |   |\    \______|   |        .
            \  /  \  /  /    ______    \  |   | \              /                                      .
 .       .   \/    \/  /____/      \____\ |___|  \____________/             .       .     
                               .                                        .
     .                           .         .               .                           .
                .                                   .            .          .       .                 .
"
credits="droritzz and YAMS"
source_code="source code: https://gitlab.com/mondbev/AMS"
scroll(){
    while read -r ; do echo "$REPLY" ; sleep $1 ; done
}
deco(){ 
    printf "%s" "$plot1" | scroll 0.3                                                    
    printf "%b" "$plot2" | scroll 0.5
    printf "%s" "$plot3" | scroll 0.3
    printf "\n%s\n%s\n" "$credits" "$source_code"
    exit 0
}
deco
